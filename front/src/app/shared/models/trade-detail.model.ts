export interface TradeDetail {
  ID: number;
  CreatedAt?: Date;
  trade_id: number;
  buy_price: string | number;
  sell_price: string | number;
  buy_executed_qty: string | number;
  sell_executed_qty: string | number;
}
