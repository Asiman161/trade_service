import { Observable } from 'rxjs';

/**
 * Data for {@link ConfirmDialogComponent}.
 * @prop {Observable<string>} title
 * @prop {Observable<string>} content
 * @prop {Observable<string>} acceptButtonText
 * @prop {Observable<string>} closeButtonText
 */
export interface ConfirmDialogData {
  title: Observable<string>;
  content: Observable<string>;
  acceptButtonText?: Observable<string>;
  closeButtonText?: Observable<string>;
}
