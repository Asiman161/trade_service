import { TradeDetail } from '@shared/models/trade-detail.model';

export interface Trade {
  ID: number;
  CreatedAt?: Date;
  symbol: string;
  side: string;
  look_by: string;
  take_position: string;
  stop_position: string;
  price: string;
  quantity: string;
  buy_direction: string;
  type: string;
  close_by_stop: boolean;
  is_active: boolean;
  is_done: boolean;
  is_cancel: boolean;
  placed_orders: any;
  trade_detail: TradeDetail;
  user_id: number;
  trade_detail_id: number;
}
