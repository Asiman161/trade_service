export enum Time {
  // in milliseconds
  MilliSeconds = 1000,
  // in seconds
  Seconds = 59,
  Minute = 60,
  Minutes = 3599,
  Hour = 3600,
  Hours = 86399,
  Day = 86400,
  Days = 2678399,
  Month = 2678400,
  Months = 32140799,
  Year = 32140800,
}
