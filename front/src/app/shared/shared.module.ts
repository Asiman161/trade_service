import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import {
  MatButtonModule, MatIconModule, MatListModule, MatCardModule,
  MatDividerModule, MatSidenavModule, MatToolbarModule, MatMenuModule,
  MatSnackBarModule, MatInputModule, MatExpansionModule, MatDialogModule, MatSelectModule, MatAutocompleteModule,
  MatCheckboxModule, MatSlideToggleModule, MatProgressBarModule, MatTableModule,
} from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';

import { DashboardLayoutComponent } from './components/dashboard-layout/dashboard-layout.component';
import { TimeAgoPipe } from './pipes/time-ago.pipe';
import { TimeIntervalPipe } from './pipes/time-interval.pipe';
import { NamePipe } from './pipes/name.pipe';
import { ConfirmDialogComponent } from '@shared/components/confirm-dialog/confirm-dialog.component';
import { UserSettingsService } from '@shared/services/user-settings.service';
import { TradesListService } from '@shared/services/trades-list.service';

const ANGULAR_MODULES = [
  FormsModule,
  ReactiveFormsModule,
  CommonModule,
];

const MATERIAL_MODULES = [
  MatButtonModule, MatIconModule, MatListModule, MatCardModule,
  MatDividerModule, MatSidenavModule, MatToolbarModule, MatMenuModule,
  MatSnackBarModule, MatInputModule, MatExpansionModule, MatDialogModule,
  MatSelectModule, MatAutocompleteModule, MatCheckboxModule, MatSlideToggleModule,
  MatProgressBarModule, MatTableModule,
];

const COVALENT_MODULES = [];

const SERVICES = [UserSettingsService, TradesListService];

const OTHER_LIBS = [
  TranslateModule,
];

const PIPES = [
  TimeAgoPipe, TimeIntervalPipe, NamePipe,
];

const DIALOGUES = [ConfirmDialogComponent];

const COMPONENTS = [
  DashboardLayoutComponent,
  ...DIALOGUES,
];

@NgModule({
  imports: [
    ANGULAR_MODULES,
    MATERIAL_MODULES,
    COVALENT_MODULES,
    OTHER_LIBS,
    RouterModule,
  ],
  exports: [
    ANGULAR_MODULES,
    MATERIAL_MODULES,
    COVALENT_MODULES,
    OTHER_LIBS,
    PIPES,
    COMPONENTS,
  ],
  declarations: [PIPES, COMPONENTS],
  entryComponents: [DIALOGUES],
  providers: [SERVICES],
})
export class SharedModule {
}
