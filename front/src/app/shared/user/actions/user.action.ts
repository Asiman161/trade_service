import { Action } from '@ngrx/store';

import { IUser } from '@shared/user/models/user.model';

export const SET_USER = '[User] Set';
export const DROP_USER = '[User] Drop';

export class SetUser implements Action {
  readonly type = SET_USER;

  constructor(public payload: IUser) {}
}

export class DropUser implements Action {
  readonly type = DROP_USER;
}

export type All
  = SetUser
  | DropUser;
