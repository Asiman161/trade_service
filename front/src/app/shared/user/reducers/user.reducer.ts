import { IUser } from '@shared/user/models/user.model';
import * as UserActions from '../actions/user.action';

export type Action = UserActions.All;

const defaultUser: IUser = undefined;

const newState = (state: IUser, newData: IUser): IUser => {
  return Object.assign({}, state, newData);
};

export function reducer(state: IUser = defaultUser, action: Action) {
  switch (action.type) {
    case UserActions.SET_USER:
      return newState(state, action.payload);
    case UserActions.DROP_USER:
      return defaultUser;
    default:
      return state;
  }
}
