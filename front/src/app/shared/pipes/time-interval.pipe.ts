import { Pipe, PipeTransform } from '@angular/core';

import { Time } from '@shared/constants/time.enum';

@Pipe({
  name: 'timeInterval',
})
export class TimeIntervalPipe implements PipeTransform {

  transform(value: Date, args?: any): string {
    const date = (Date.now() - new Date(value).getTime()) / Time.MilliSeconds;
    switch (true) {
      case date <= Time.Seconds:
        return 'SECONDS';
      case date <= Time.Minutes:
        return 'MINUTES';
      case date <= Time.Hours:
        return 'HOURS';
      case date <= Time.Days:
        return 'DAYS';
      case date <= Time.Months:
        return 'MONTHS';
      default:
        return 'YEARS';
    }
  }
}
