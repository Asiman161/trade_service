import { Pipe, PipeTransform } from '@angular/core';
declare const _;

@Pipe({
  name: 'name'
})
export class NamePipe implements PipeTransform {

  transform(values: string[], value: string): string[] {
    return values.filter(val => new RegExp(`${value}`, 'gi').test(val));
  }

}
