import { Pipe, PipeTransform } from '@angular/core';

import { Time } from '@shared/constants/time.enum';

@Pipe({
  name: 'timeAgo',
})
export class TimeAgoPipe implements PipeTransform {

  transform(value: Date, args?: any): number {
    const date = (Date.now() - new Date(value).getTime()) / Time.MilliSeconds;
    switch (true) {
      case date <= Time.Seconds:
        return Math.floor(date / Time.MilliSeconds);
      case date <= Time.Minutes:
        return Math.floor(date / Time.MilliSeconds / Time.Minute) || 1;
      case date <= Time.Hours:
        return Math.floor(date / Time.MilliSeconds / Time.Hour) || 1;
      case date <= Time.Days:
        return Math.floor(date / Time.MilliSeconds / Time.Day) || 1;
      case date <= Time.Months:
        return Math.floor(date / Time.MilliSeconds / Time.Month) || 1;
      default:
        return Math.floor(date / Time.MilliSeconds / Time.Year) || 1;
    }
  }

}
