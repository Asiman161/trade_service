import { Component, Inject } from '@angular/core';

import { MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

import { ConfirmDialogData } from '@shared/models/confirm-dialog-data.model';


/**
 * Customizable abstract confirm dialog.
 * Have default values for {@link ConfirmDialogData.closeButtonText} and {@link ConfirmDialogData.acceptButtonText}.
 * @see {@link ConfirmDialogData}
 * @see {@link MatDialog}
 */
@Component({
  selector: 'ts-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent {

  public _defaultCloseButtonText = this._translate.get('ACTIONS.NO');
  public _defaultAcceptButtonText = this._translate.get('ACTIONS.YES');

  constructor(@Inject(MAT_DIALOG_DATA) public data: ConfirmDialogData,
              private _translate: TranslateService) {}
}
