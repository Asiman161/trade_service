import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { MatSnackBar } from '@angular/material';
import { DeviceDetectorService } from 'ngx-device-detector';

import { IUser } from '@shared/user/models/user.model';
import { TranslatesService } from '@shared/translates/translates.service';
import { AuthenticationService } from '../../../authentication/shared/services/authentication.service';

@Component({
  selector: 'ts-dashboard-layout',
  templateUrl: './dashboard-layout.component.html',
  styleUrls: ['./dashboard-layout.component.scss'],
})
export class DashboardLayoutComponent implements OnInit {

  @Input() public title: string;
  @Input() public user: IUser;
  public sideOpen = true;
  public isMobile = false;

  constructor(private _router: Router,
              private _snackBar: MatSnackBar,
              private _translatesService: TranslatesService,
              private _authenticationService: AuthenticationService,
              private deviceService: DeviceDetectorService) { }

  ngOnInit() {
    this.isMobile = this.deviceService.isMobile();
    if (this.isMobile) {
      this.sideOpen = false;
    }
  }

  public changeLang(lang): void {
    this._translatesService.use(lang);
  }

  public signOut(): void {
    this._authenticationService.signOut().subscribe(() => {
      this._router.navigate(['/']);
    }, () => {
      this._translatesService.get(['ERRORS.UNKNOWN', 'ACTIONS.CLOSE']).subscribe((translates) => {
        this._snackBar.open(translates['ERRORS.UNKNOWN'], translates['ACTIONS.CLOSE'], {
          duration: 3000,
        });
      });
      this._router.navigate(['/']);
    });
  }
}
