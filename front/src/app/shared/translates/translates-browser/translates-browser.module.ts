import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TransferState } from '@angular/platform-browser';

import { TranslateLoader, TranslateModule, TranslateParser } from '@ngx-translate/core';
import { TranslateICUParser } from 'ngx-translate-parser-plural-select';

import { TranslatesBrowserLoaderService } from './translates-browser-loader.service';
import { TranslatesService } from '@shared/translates/translates.service';

export function translateStaticLoader(http: HttpClient, transferState: TransferState): TranslatesBrowserLoaderService {
  return new TranslatesBrowserLoaderService('/assets/i18n/', '.json', transferState, http);
}

@NgModule({
  imports: [
    TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: translateStaticLoader,
          deps: [HttpClient, TransferState],
        },
        parser: {
          provide: TranslateParser,
          useClass: TranslateICUParser,
        },
      },
    ),
  ],
  providers: [TranslatesService],
})
export class TranslatesBrowserModule {
}
