import { NgModule } from '@angular/core';
import { TransferState } from '@angular/platform-browser';

import { TranslateLoader, TranslateModule, TranslateParser } from '@ngx-translate/core';
import { TranslateICUParser } from 'ngx-translate-parser-plural-select';

import { TranslatesServerLoaderService } from './translates-server-loader.service';
import { TranslatesService } from '@shared/translates/translates.service';


export function translateFactory(transferState: TransferState): TranslatesServerLoaderService {
  return new TranslatesServerLoaderService('./dist/assets/i18n', '.json', transferState);
}

@NgModule({
  imports: [
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translateFactory,
        deps: [TransferState]
      },
      parser: {
        provide: TranslateParser,
        useClass: TranslateICUParser
      },
    }),
  ],
  providers: [TranslatesService]
})
export class TranslatesServerModule {
}
