import { Injectable, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { AppStorage } from 'forStorage/universal.inject';

import { ITranslatesLanguage } from './translates.interface';
import { Observable } from 'rxjs/Observable';

const LANGUAGES: ITranslatesLanguage[] = [
  { value: 'ru', name: 'Русский' },
  { value: 'en', name: 'English' },
];
const LANG_LIST: string[] = ['ru', 'en'];
const DEFAULT_LANG = 'ru';

@Injectable()
export class TranslatesService {
  constructor(private _translateService: TranslateService,
              @Inject(AppStorage) private _appStorage: Storage) {
    this._translateService.addLangs(LANG_LIST);
    this._translateService.setDefaultLang(DEFAULT_LANG);
    this._translateService.use(this._appStorage.getItem('lang') || DEFAULT_LANG);
  }

  public getLanguages(): ITranslatesLanguage[] {
    return LANGUAGES;
  }

  public getCurrentLang(): string {
    return this._translateService.currentLang;
  }

  public get(key: string | string[]): Observable<string | any> {
    return this._translateService.get(key);
  }

  public use(lang: string): void {
    this._appStorage.setItem('lang', lang);
    this._translateService.use(lang);
  }
}
