import { FormArray, FormGroup } from '@angular/forms';

export class Helpers {

  public static CURRENCY_PATTERN = /^(\d{1,15}\.\d{1,15}|(?!0)\d{1,15})$/;
  /**
   * Loop and touch all it has
   * @param {FormGroup} formGroup
   * @param func << function name: [markAsTouched, markAsUntouched, markAsDirty, markAsPristine, markAsPending
   * @param opts
   */
  public static touchAll(formGroup: FormGroup | FormArray, func = 'markAsDirty', opts = { onlySelf: false }): void {
    for (const c in formGroup.controls) {
      if (formGroup.controls[c] instanceof FormGroup || formGroup.controls[c] instanceof FormArray) {
        Helpers.touchAll(formGroup.controls[c], func, opts);
      } else {
        formGroup.controls[c][func](opts);
      }
    }
  }
}
