import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { APP_BASE_HREF } from '@angular/common';

@Injectable()
export class PredictionsService {

  constructor(@Inject(APP_BASE_HREF) private _origin: string,
    private _http: HttpClient) { }

  // TODO: Create model instead of 'any'
  public getPredictions(): Observable<any> {
    return this._http.get(`${this._origin}/api/predictions`).pipe();
  }

  public createPredict(data: any): Observable<any> {
    return this._http.post(`${this._origin}/api/predictions/create`, data).pipe();
  }

  public getTradePairs(): Observable<any> {
    return this._http.get(`${this._origin}/api/trade-pairs`).pipe();
  }

  public createOrder(data: any): Observable<any> {
    return this._http.post(`${this._origin}/api/trades/`, data).pipe();
  }
}
