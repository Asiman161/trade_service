import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { APP_BASE_HREF } from '@angular/common';
import { Trade } from '@shared/models/trade.model';

@Injectable()
export class TradesListService {

  constructor(@Inject(APP_BASE_HREF) private _origin: string,
              private _http: HttpClient) { }

  getTrades(): Observable<Trade[]> {
    return this._http.get<Trade[]>(`${this._origin}/api/trades/`).pipe();
  }

  cancelTrade(id: number): Observable<Trade> {
    return this._http.delete<Trade>(`${this._origin}/api/trades/${id}/`).pipe();
  }
}
