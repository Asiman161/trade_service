import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_BASE_HREF } from '@angular/common';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserSettingsService {

  constructor(@Inject(APP_BASE_HREF) private _origin: string,
              private _http: HttpClient) { }

  // TODO: Create model instead of 'any'
  public getAPIKeys(): Observable<any> {
    return this._http.get(`${this._origin}/api/users/`).pipe();
  }

  public updateAPIData(data): Observable<any> {
    return this._http.patch(`${this._origin}/api/users/`, data).pipe();
  }

  deleteAPIKeys(): Observable<any> {
    return this._http.delete(`${this._origin}/api/users/`).pipe();
  }
}
