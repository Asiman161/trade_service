import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { routes } from './landing.routes';
import { LandingComponent } from './landing.component';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
    CommonModule,
  ],
  exports: [],
  declarations: [LandingComponent],
  providers: [],
})
export class LandingModule {
}
