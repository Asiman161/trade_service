import { Component, Inject, OnDestroy, OnInit } from '@angular/core';

import { TranslatesService } from '@shared/translates/translates.service';
import { Store } from '@ngrx/store';

import { Subscription } from 'rxjs/Subscription';

import { AuthenticationService } from '@shared/../authentication/shared/services/authentication.service';
import { AppStorage } from '../../forStorage/universal.inject';
import { IToken } from '../authentication/shared/models/token.model';

interface AppState {
  token: IToken;
}

@Component({
  selector: 'ts-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
})
export class LandingComponent implements OnInit, OnDestroy {

  public token: string;
  public tokenSubscribe: Subscription;

  constructor(@Inject(AppStorage) private _appStorage: Storage,
              private _store: Store<AppState>,
              private _translatesService: TranslatesService,
              private _authenticationService: AuthenticationService) {
    this.tokenSubscribe = this._store
      .select(state => state.token)
      .subscribe(token => { this.token = token.token; });
  }

  ngOnInit(): void {
  }

  public signOut(): void {
    this._authenticationService.signOut().subscribe(() => {
    }, () => {

    });
  }

  public changeLang(lang): void {
    this._translatesService.use(lang);
  }

  ngOnDestroy(): void {
    this.tokenSubscribe.unsubscribe();
  }
}
