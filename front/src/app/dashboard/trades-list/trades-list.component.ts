import { Component, OnInit } from '@angular/core';

import { TradesListService } from '@shared/services/trades-list.service';
import { Trade } from '@shared/models/trade.model';

@Component({
  selector: 'ts-trades-list',
  templateUrl: './trades-list.component.html',
  styleUrls: ['./trades-list.component.scss'],
})
export class TradesListComponent implements OnInit {

  trades: Trade[];
  displayedColumns: string[] = [
    'CreatedAt',
    'symbol',
    'take_position',
    'stop_position',
    'price',
    'quantity',
    'status',
    'ID',
  ];

  constructor(private _tradesList: TradesListService) { }

  ngOnInit(): void {
    this._tradesList.getTrades().subscribe(res => {
      this.trades = res;
    });
  }

  cancelTrade(row: Trade): void {
    this._tradesList.cancelTrade(row.ID).subscribe(res => {
      Object.assign(row, res);
    });
  }
}
