import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { routes } from './dashboard.routes';
import { SharedModule } from '@shared/shared.module';
import { DashboardComponent } from './dashboard.component';
import { PredictionsComponent } from './predictions/predictions.component';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { PredictionsModule } from './predictions/predictions.module';
import { TradesListComponent } from './trades-list/trades-list.component';

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    PredictionsModule,
  ],
  declarations: [
    DashboardComponent,
    PredictionsComponent,
    UserSettingsComponent,
    TradesListComponent,
  ],
})

export class DashboardModule {
}
