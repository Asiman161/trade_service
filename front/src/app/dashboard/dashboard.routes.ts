import { Routes } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { PredictionsComponent } from './predictions/predictions.component';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { TradesListComponent } from './trades-list/trades-list.component';

export const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: '',
        component: PredictionsComponent,
        data: { title: 'PAGES.PREDICTIONS.TITLE' },
      }, {
        path: 'settings',
        component: UserSettingsComponent,
        data: { title: 'PAGES.ACCOUNT_SETTINGS.TITLE' },
      },
      {
        path: 'trades-list',
        component: TradesListComponent,
        data: { title: 'PAGES.TRADES_LIST.TITLE' },
      },
    ],
  },
];
