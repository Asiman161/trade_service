import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';

import { IUser } from '@shared/user/models/user.model';

interface AppState {
  user: IUser;
}

@Component({
  selector: 'ts-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {

  public title: string;
  public user: IUser;
  public navmenu = [
    {
      icon: 'looks_one',
      route: '/dashboard',
      title: 'PAGES.PREDICTIONS.TITLE',
      description: 'PAGES.PREDICTIONS.DESCRIPTION',
    },
    {
      icon: 'looks_two',
      route: 'trades-list',
      title: 'PAGES.TRADES_LIST.TITLE',
      description: 'PAGES.TRADES_LIST.DESCRIPTION',
    }
  ];

  private _userSubscribe: Subscription;
  private _currentRouteSubscription: Subscription;

  constructor(private _store: Store<AppState>,
              private _router: Router,
              private _route: ActivatedRoute) { }

  ngOnInit(): void {
    this._userSubscribe = this._store
      .select(state => state.user)
      .subscribe(user => this.user = user);
    if (!this._currentRouteSubscription) {
      const child: ActivatedRoute = this._route.firstChild;
      if (child) {
        this.title = child.snapshot.data['title'];
      }
    }
    this._currentRouteSubscription = this._router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        this.title = this._route.snapshot.data['title'];
        const child: ActivatedRoute = this._route.firstChild;
        if (child) {
          this.title = child.snapshot.data['title'];
        }
      }
    });
  }

  ngOnDestroy(): void {
    this._currentRouteSubscription.unsubscribe();
    this._userSubscribe.unsubscribe();
  }
}
