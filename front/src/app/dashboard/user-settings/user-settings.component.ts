import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';

import { ConfirmDialogComponent } from '@shared/components/confirm-dialog/confirm-dialog.component';
import { TranslatesService } from '@shared/translates/translates.service';
import { UserSettingsService } from '@shared/services/user-settings.service';

@Component({
  selector: 'ts-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss'],
})
export class UserSettingsComponent implements OnInit {

  userDetailForm: FormGroup;
  editing = false;
  userDetailCache: any;
  showWarningAPIAdded = false;

  constructor(private _fb: FormBuilder,
              private _dialog: MatDialog,
              private _toastr: ToastrService,
              private _userSettingsService: UserSettingsService,
              private _translatesService: TranslatesService) { }

  ngOnInit(): void {
    this.initForms();

    this.getAPIKeys();
  }

  initForms(): void {
    this.userDetailForm = this._fb.group({
      key: [
        {
          value: '',
          disabled: true,
        }, [Validators.minLength(10), Validators.required, Validators.pattern(/^[a-zA-Z0-9]*$/)],
      ],
      secret: [
        {
          value: '',
          disabled: true,
        }, [Validators.minLength(10), Validators.required, Validators.pattern(/^[a-zA-Z0-9]*$/)],
      ],
    });
  }

  submit(): void {
    if (this.userDetailForm.valid) {
      const data = this.userDetailForm.getRawValue();
      this._userSettingsService.updateAPIData(data).subscribe(res => {
        this.userDetailForm.setValue(res);
        this.showWarningAPIAdded = true;
        this.editing = false;
        this._disableForm();
        this._translatesService.get('TOASTR.SUCCESS.TITLE').subscribe(translate => {
          this._toastr.success(translate);
        });
      }, err => {
        this._translatesService.get('TOASTR.ERROR.TITLE').subscribe(translate => {
          this._toastr.error(translate);
        });
      });
    }
  }

  getAPIKeys(): void {
    this._userSettingsService.getAPIKeys().subscribe(res => {
      this.userDetailForm.setValue(res);
    });
  }

  cancel(): void {
    this.editing = false;
    this.userDetailForm.setValue(this.userDetailCache);
    this._disableForm();
  }

  changeAPIKeys(): void {
    this.editing = true;
    this.userDetailCache = this.userDetailForm.getRawValue();
    this.userDetailForm.patchValue({ secret: '' });
    this._enableForm();
  }

  deleteAPIKeys(): void {
    const dialog = this._dialog.open(ConfirmDialogComponent, {
      width: '300px',
      data: {
        title: this._translatesService.get('ACTIONS.DELETING_API_KEYS'),
        content: this._translatesService.get('PHRASES.DELETING_API_KEYS_DIALOG_INFO'),
      },
    });

    dialog.afterClosed().subscribe(dialogRes => {
      if (!!dialogRes) {
        this._userSettingsService.deleteAPIKeys().subscribe(res => {
          this.showWarningAPIAdded = false;
          this._translatesService.get('TOASTR.SUCCESS.TITLE').subscribe(translate => {
            this.userDetailForm.reset();
            this._toastr.success(translate);
          });
        }, err => {
          this._translatesService.get('TOASTR.ERROR.TITLE').subscribe(translate => {
            this._toastr.success(translate);
          });
        });
      }
    });
  }

  private _enableForm(): void {
    for (const key of Object.keys(this.userDetailForm.controls)) {
      this.userDetailForm.controls[key].enable();
    }
  }

  private _disableForm(): void {
    for (const key of Object.keys(this.userDetailForm.controls)) {
      this.userDetailForm.controls[key].disable();
    }
  }
}
