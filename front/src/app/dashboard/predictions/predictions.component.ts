import { Component, OnDestroy, OnInit } from '@angular/core';

import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';

import { PredictionsService } from '@shared/services/predictions.service';
import { IPredictionCard } from './prediction.model';
import { PredictionDetailComponent } from './prediction-detail/prediction-detail.component';
import { PredictionCreateComponent } from './prediction-create/prediction-create.component';
import { IUser } from '@shared/user/models/user.model';

interface AppState {
  user: IUser;
}

@Component({
  selector: 'ts-predictions',
  templateUrl: './predictions.component.html',
  styleUrls: ['./predictions.component.scss'],
})
export class PredictionsComponent implements OnInit, OnDestroy {

  public items: IPredictionCard[];
  public user: IUser;
  private _dialogSubscribe: Subscription;
  private _userSubscribe: Subscription;

  constructor(private _store: Store<AppState>,
              public dialog: MatDialog,
              private _predictionsService: PredictionsService) {

  }

  ngOnInit() {
    this._userSubscribe = this._store
      .select(state => state.user)
      .subscribe(user => this.user = user);
    this._predictionsService.getPredictions().subscribe((res: IPredictionCard[]) => {
      this.items = res;
    });
  }

  public openPrediction(data: IPredictionCard) {
    this.dialog.open(PredictionDetailComponent, {
      data: data,
      width: 'calc(100% - 30px)',
      maxWidth: '1024px'
    });
  }

  public createPrediction() {
    const dialog = this.dialog.open(PredictionCreateComponent, {
      width: 'calc(100% - 30px)',
      maxWidth: '1024px'
    });
    this._dialogSubscribe = dialog.afterClosed().subscribe((item: IPredictionCard) => {
      this._dialogSubscribe.unsubscribe();
      if (item) {
      this.items.push(item);
      }
    });
  }


  ngOnDestroy(): void {
    if (this._dialogSubscribe) {
      this._dialogSubscribe.unsubscribe();
    }
    this._userSubscribe.unsubscribe();
  }
}
