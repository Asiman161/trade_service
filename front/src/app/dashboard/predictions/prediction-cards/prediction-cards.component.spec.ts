import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PredictionCardsComponent } from './prediction-cards.component';

describe('PredictionCardsComponent', () => {
  let component: PredictionCardsComponent;
  let fixture: ComponentFixture<PredictionCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PredictionCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PredictionCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
