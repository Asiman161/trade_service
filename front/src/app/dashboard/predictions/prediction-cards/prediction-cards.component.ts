import {
  AfterViewInit, Component, EventEmitter, HostListener, Inject, Input, OnInit, Output, PLATFORM_ID,
  ViewContainerRef,
} from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { IPredictionCard } from '../prediction.model';

@Component({
  selector: 'ts-prediction-cards',
  templateUrl: './prediction-cards.component.html',
  styleUrls: ['./prediction-cards.component.scss'],
})

export class PredictionCardsComponent implements OnInit, AfterViewInit {

  @Input()
  public set items(items) {
    this.cards = items;
    this.resizeAllGridItems();
  }

  @Output() public openPrediction: EventEmitter<IPredictionCard> = new EventEmitter();

  public cards: IPredictionCard[];
  private _allCardsHTMLElements: NodeList[];

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.resizeAllGridItems();
  }

  constructor(@Inject(PLATFORM_ID) private _platformId: Object,
              private _vcr: ViewContainerRef) { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.resizeAllGridItems();
  }

  // TODO: optimize. Right now it will calls for every new card
  resizeGridItem(item: HTMLElement) {
    if (isPlatformBrowser(this._platformId)) {
      const computedStyle = getComputedStyle(this._vcr.element.nativeElement);
      const rowHeight = parseInt(computedStyle.getPropertyValue('grid-auto-rows'), 10);
      const rowGap = parseInt(computedStyle.getPropertyValue('grid-row-gap'), 10);
      const rowSpan = Math.ceil((item.querySelector('mat-card')
        .getBoundingClientRect().height + rowGap) / (rowHeight + rowGap));
      item.style['gridRowEnd'] = 'span ' + rowSpan;
    } else if (isPlatformServer(this._platformId)) {
      item.style['gridRowEnd'] = 'span ' + 40;
    }

  }

  resizeAllGridItems() {
      this._allCardsHTMLElements = this._vcr.element.nativeElement.querySelectorAll('.card__wrapper');
    for (let i = 0; i < this._allCardsHTMLElements.length; i++) {
      this.resizeGridItem(this._allCardsHTMLElements[i] as any);
    }
  }

  onOpenPrediction(data: IPredictionCard) {
    this.openPrediction.emit(data);
  }
}
