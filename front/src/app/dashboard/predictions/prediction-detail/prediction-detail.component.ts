import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';

import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { TranslatesService } from '@shared/translates/translates.service';

import { IPrediction } from '../prediction.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Helpers } from '@shared/helpers/helpers';
import { PredictionsService } from '@shared/services/predictions.service';


@Component({
  selector: 'ts-prediction-detail',
  templateUrl: './prediction-detail.component.html',
  styleUrls: ['./prediction-detail.component.scss'],
})
// TODO: rename to PredictionDetailDialogComponent
export class PredictionDetailComponent implements OnInit {

  newTrade = false;
  orderForm: FormGroup;
  @ViewChild('form', { read: ElementRef }) private _form: ElementRef;

  constructor(@Inject(MAT_DIALOG_DATA) public prediction: IPrediction,
              private dialogRef: MatDialogRef<PredictionDetailComponent>,
              private _fb: FormBuilder,
              private _predictionsService: PredictionsService,
              private _toastr: ToastrService,
              private _translatesService: TranslatesService) { }

  ngOnInit(): void {
    this._initForm();
  }

  scrollToOrderForm(): void {
    setTimeout(() => {
      this._form.nativeElement.scrollIntoView({ block: 'start' });
    });
  }

  submit(): void {
    if (this.orderForm.valid) {
      const data = this.orderForm.getRawValue();
      data.buy_direction = data.buy_direction ? 'UP' : 'DOWN';
      this._predictionsService.createOrder(data).subscribe(res => {
        this._translatesService.get('TOASTR.SUCCESS.TITLE').subscribe(translate => {
          this._toastr.success(translate);
          this.dialogRef.close();
        });
      }, err => {
        this._translatesService.get('TOASTR.ERROR.TITLE').subscribe(translate => {
          this._toastr.error(translate);
        });
      });
    } else {
      Helpers.touchAll(this.orderForm, 'markAsTouched');
    }
  }

  cancel(): void {
    this.newTrade = false;
  }

  private _initForm(): void {
    this.orderForm = this._fb.group({
      exchange: [
        {
          value: this.prediction.exchange.name,
          disabled: true,
        },
      ],
      symbol: [
        {
          value: this.prediction.trade_pair.pair,
          disabled: true,
        },
      ],
      side: 'BUY',
      look_by: 'CURRENT',
      take_position: [
        this.prediction.targets.target_zones[0].tz_min, [
          Validators.required,
          Validators.pattern(Helpers.CURRENCY_PATTERN),
        ],
      ],
      stop_position: [
        this.prediction.targets.stop, [
          Validators.required,
          Validators.pattern(Helpers.CURRENCY_PATTERN),
        ],
      ],
      price: [
        '', [
          Validators.required,
          Validators.pattern(Helpers.CURRENCY_PATTERN),
        ],
      ],
      quantity: ['', [Validators.required]],
      buy_direction: [this.prediction.order_type],
      type: 'MARKET',
    });
  }
}
