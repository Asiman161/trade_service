import { IUser } from '@shared/user/models/user.model';

export interface IPredictionCard {
  img_url: string;
  order_type: boolean;
  views_count: number;
  comments_count: number;
  likes_count: number;
  short_description: string;
  trade_pair: {
    pair: string;
  };
  exchange: {
    name: string;
  };
  description: string;
  user: IUser;
  created_at: Date;
  modified_at: Date;
}

export interface IPrediction extends IPredictionCard {
  targets: {
    stop: string,
    target_zones: [
      {
        description: string;
        tz_min: string;
        tz_max?: string;
      }
      ]
  };
}
