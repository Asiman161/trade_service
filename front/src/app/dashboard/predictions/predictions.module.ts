import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared/shared.module';
import { PredictionCardsComponent } from './prediction-cards/prediction-cards.component';
import { PredictionsService } from '@shared/services/predictions.service';
import { PredictionDetailComponent } from './prediction-detail/prediction-detail.component';
import { PredictionCreateComponent } from './prediction-create/prediction-create.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
  ],
  declarations: [
    PredictionCardsComponent,
    PredictionDetailComponent,
    PredictionCreateComponent,
  ],
  exports: [
    PredictionCardsComponent,
  ],
  entryComponents: [PredictionDetailComponent, PredictionCreateComponent],
  providers: [PredictionsService],
})

export class PredictionsModule {
}
