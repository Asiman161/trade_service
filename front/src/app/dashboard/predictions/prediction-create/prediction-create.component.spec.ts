import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PredictionCreateComponent } from './prediction-create.component';

describe('PredictionCreateComponent', () => {
  let component: PredictionCreateComponent;
  let fixture: ComponentFixture<PredictionCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PredictionCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PredictionCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
