import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { MatDialogRef } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

import { PredictionsService } from '@shared/services/predictions.service';
import { Helpers } from '@shared/helpers/helpers';
import { TranslatesService } from '@shared/translates/translates.service';

@Component({
  selector: 'ts-prediction-create',
  templateUrl: './prediction-create.component.html',
  styleUrls: ['./prediction-create.component.scss'],
})
export class PredictionCreateComponent implements OnInit {

  public predictionForm: FormGroup;

  private _currencyPattern = Helpers.CURRENCY_PATTERN;

  public exchanges = [];

  constructor(private _fb: FormBuilder,
              private _toastr: ToastrService,
              private _translatesService: TranslatesService,
              private _predictionsService: PredictionsService,
              public dialogRef: MatDialogRef<PredictionCreateComponent>) { }

  ngOnInit() {
    this._predictionsService.getTradePairs().subscribe(res => {
      // TODO: Change data format. It shouldn't be so
      res.forEach((item, index) => {
        item.exchange.forEach((item2) => {
          if (this.exchanges[index]) {
            if (!this.exchanges[index].trade_pairs.includes(item.pair)) {
              this.exchanges[index].trade_pairs.push(item.pair);
            }
          } else {
            this.exchanges[index] = { name: item2.name, trade_pairs: [item.pair] };
          }
        });
      });
    });

    this._initForm();
  }

  public add_target_zone(control: FormArray): void {
    control.push(this._fb.group({
      description: ['', Validators.required],
      tz_min: ['', [Validators.required, Validators.pattern(this._currencyPattern)]],
      tz_max: ['', [Validators.pattern(this._currencyPattern)]],
    }));
  }

  public removeTargetZone(control: FormArray, index: number): void {
    control.removeAt(index);
  }

  public setExchange(exchange: string) {
    this.predictionForm.patchValue({ exchange: exchange });
  }

  public submit() {
    if (this.predictionForm.valid) {
      this._predictionsService.createPredict(this.predictionForm.getRawValue())
        .subscribe(res => {
          this.dialogRef.close(res);
        }, err => {
          this._translatesService.get(['TOASTR.ERRORS.PREDICTIONS.CREATE', 'TOASTR.ERRORS.TITLE']).subscribe(res => {
            this._toastr.error(res['TOASTR.ERRORS.PREDICTIONS.CREATE'], res['TOASTR.ERRORS.TITLE']);
          });
        });
    } else {
      Helpers.touchAll(this.predictionForm, 'markAsTouched');
    }
  }

  private _initForm() {
    this.predictionForm = this._fb.group({
      trade_pair: ['', Validators.required],
      exchange: ['', Validators.required],
      img_url: ['', Validators.maxLength(255)],
      order_type: [true],
      short_description: ['', [Validators.required, Validators.maxLength(255)]],
      description: ['', Validators.required],
      targets: this._fb.group({
        stop: ['', [Validators.required, Validators.pattern(this._currencyPattern)]],
        target_zones: this._fb.array([
          this._fb.group({
            description: ['', Validators.required],
            tz_min: ['', [Validators.required, Validators.pattern(this._currencyPattern)]],
            tz_max: ['', [Validators.pattern(this._currencyPattern)]],
          }),
        ]),
      }),
    });
  }
}
