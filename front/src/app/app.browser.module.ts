import { NgModule } from '@angular/core';
import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { APP_BASE_HREF } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { REQUEST } from '@nguniversal/express-engine/tokens';

import { AppModule } from './app.module';
import { AppComponent } from './app.component';
import { Interceptor } from './http-interceptor';
import { TranslatesBrowserModule, } from '@shared/translates/translates-browser/translates-browser.module';
import { CookieStorage } from '../forStorage/browser.storage';
import { AppStorage } from '../forStorage/universal.inject';

// the Request object only lives on the server
export function getRequest(): any {
  return { headers: { cookie: document.cookie } };
}

@NgModule({
  imports: [
    AppModule,
    BrowserAnimationsModule,
    BrowserTransferStateModule,
    TranslatesBrowserModule,
    BrowserModule.withServerTransition({ appId: 'ts-root' }),
    TransferHttpCacheModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true },
    { provide: APP_BASE_HREF, useValue: '' },
    { provide: AppStorage, useClass: CookieStorage },
    {
      // The server provides these in main.server
      provide: REQUEST,
      useFactory: (getRequest)
    }
  ],
  bootstrap: [AppComponent],
})
export class AppBrowserModule {
}
