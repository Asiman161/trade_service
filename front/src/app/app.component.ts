import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { MatIconRegistry } from '@angular/material';

import { AppStorage } from '../forStorage/universal.inject';
import * as TokenActions from './authentication/shared/actions/token.action';
import { IToken } from './authentication/shared/models/token.model';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import * as UserActions from '@shared/user/actions/user.action';
import { IUser } from '@shared/user/models/user.model';
import { AuthenticationService } from './authentication/shared/services/authentication.service';

interface AppState {
  token: IToken;
  user: IUser;
}

@Component({
  selector: 'ts-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  private token: string;

  constructor(@Inject(AppStorage) private _appStorage: Storage,
              @Inject(PLATFORM_ID) private _platformId: Object,
              private _store: Store<AppState>,
              private _translateService: TranslateService,
              private _authenticationService: AuthenticationService,
              matIconRegistry: MatIconRegistry,
              sanitizer: DomSanitizer) {
    if (isPlatformBrowser(this._platformId)) {
      matIconRegistry
        .addSvgIconSetInNamespace(
          'assets',
          sanitizer.bypassSecurityTrustResourceUrl('/assets/svg_set.svg'),
        );
    }
  }

  ngOnInit(): void {
    this._translateService.setDefaultLang('ru');
    this._translateService.use(this._appStorage.getItem('lang') || 'ru');

    if (this.token = this._appStorage.getItem('token')) {
      this._store.dispatch(new TokenActions.SetToken(this.token));
    }
    if (isPlatformBrowser(this._platformId)) {
      const user: IUser = JSON.parse(window.localStorage.getItem('user') || null);
      if (user) {
        this._store.dispatch(new UserActions.SetUser(user));
      }
    } else if (isPlatformServer(this._platformId)) {
      // TODO: get user info from server
    }
  }
}
