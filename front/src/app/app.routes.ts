import { Routes } from '@angular/router';
import { PageNotFoundComponent } from './404/page-not-found.component';
import { MetaGuard } from '@ngx-meta/core';

export const ROUTES: Routes = [
  {
    path: '', canActivate: [MetaGuard], children: [
      { path: '', loadChildren: './landing/landing.module#LandingModule' },
      { path: 'auth', loadChildren: './authentication/authentication.module#AuthenticationModule' },
      { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
    ],
  },

  { path: '404', component: PageNotFoundComponent },
  { path: '**', redirectTo: '/404' },
];
