import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ts-page-not-found-component',
  templateUrl: 'page-not-found-component.component.html',
})

export class PageNotFoundComponent implements OnInit {
  constructor() { }

  ngOnInit() { }
}
