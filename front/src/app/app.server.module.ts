import { NgModule } from '@angular/core';
import { ServerModule, ServerTransferStateModule } from '@angular/platform-server';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ModuleMapLoaderModule } from '@nguniversal/module-map-ngfactory-loader';
import { APP_BASE_HREF } from '@angular/common';

import { AppModule } from './app.module';
import { TranslatesServerModule } from '@shared/translates/translates-server/translates-server.module';
import { UniversalStorage } from '../forStorage/server.storage';
import { AppComponent } from './app.component';
import { AppStorage } from '../forStorage/universal.inject';

@NgModule({
  imports: [
    AppModule,
    NoopAnimationsModule,
    ServerTransferStateModule,
    ServerModule,
    ModuleMapLoaderModule,
    TranslatesServerModule,
  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: APP_BASE_HREF, useValue: 'http://back' },
    { provide: AppStorage, useClass: UniversalStorage },
  ],
})
export class AppServerModule {
}
