import { Inject, Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

import { IToken } from './authentication/shared/models/token.model';
import { AppStorage } from '../forStorage/universal.inject';
import * as TokenActions from './authentication/shared/actions/token.action';

interface AppState {
  token: IToken;
}

@Injectable()
export class Interceptor implements HttpInterceptor {

  private token: string;

  constructor(@Inject(AppStorage) private _appStorage: Storage,
              private _store: Store<AppState>,
              private _router: Router) {
    this._store
      .select(state => state.token)
      .subscribe(token => this.token = token.token);
  }

  intercept(req: HttpRequest<any>,
            next: HttpHandler): Observable<HttpEvent<any>> {
    const duplicate = req.clone({
      setHeaders: {
        Authorization: this.token,
      }
    });
    return next.handle(duplicate).pipe(
      catchError((err) => {
        if ((err.error.detail === 'Signature has expired.' || err.error.detail === 'Error decoding signature.')
          && err.status === 401) {
          this._appStorage.removeItem('token');
          this._store.dispatch(new TokenActions.DropToken());
          this._router.navigate(['/']);
        }
        return throwError(err);
      }),
    );
  }
}
