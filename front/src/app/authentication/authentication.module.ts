import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { routes } from './authentication.routes';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SharedModule } from '@shared/shared.module';
import { AuthenticationComponent } from './authentication.component';

@NgModule({
  declarations: [
    SignInComponent,
    SignUpComponent,
    AuthenticationComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
  ],
})

export class AuthenticationModule {
}
