import { IUser } from '@shared/user/models/user.model';

export interface ISignIn {
  email: string;
  password: string;
}

export interface ISignUp extends ISignIn {
  confirmPassword: string;
}

export interface IUserResponse {
  token: string;
  user: IUser;
}
