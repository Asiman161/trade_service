import { IToken } from '../models/token.model';
import * as TokenActions from '../actions/token.action';

export type Action = TokenActions.All;

const defaultState: IToken = {
  token: '',
};

const newState = (state: IToken, newData: IToken): IToken => {
  return Object.assign({}, state, newData);
};

export function reducer(state: IToken = defaultState, action: Action) {
  switch (action.type) {
    case TokenActions.SET_TOKEN:
      return newState(state, { token: `JWT ${action.payload}` });
    case TokenActions.DROP_TOKEN:
      return defaultState;
    default:
      return state;
  }
}
