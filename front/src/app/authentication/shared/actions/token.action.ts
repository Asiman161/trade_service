import { Action } from '@ngrx/store';

export const SET_TOKEN = '[Token] Set';
export const DROP_TOKEN = '[Token] Drop';

export class SetToken implements Action {
  readonly type = SET_TOKEN;

  constructor(public payload: string) {}
}

export class DropToken implements Action {
  readonly type = DROP_TOKEN;
}

export type All
  = SetToken
  | DropToken;
