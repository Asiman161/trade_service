import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_BASE_HREF, isPlatformBrowser } from '@angular/common';

import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { ISignIn, ISignUp } from '../models/authentication.model';
import { IToken } from '../models/token.model';
import * as TokenActions from '../actions/token.action';
import * as UserActions from 'app/shared/user/actions/user.action';
import { AppStorage } from '../../../../forStorage/universal.inject';
import { IUser } from 'app/shared/user/models/user.model';

interface AppState {
  token: IToken;
  user: IUser;
}

@Injectable()
export class AuthenticationService {
  constructor(@Inject(APP_BASE_HREF) private _origin: string,
              @Inject(AppStorage) private _appStorage: Storage,
              @Inject(PLATFORM_ID) private _platformId: Object,
              private _store: Store<AppState>,
              private _http: HttpClient) {
  }

  public signIn(credentials: ISignIn): Observable<any> {
    return this._http.post(`${this._origin}/api/auth/sign-in/`, credentials).pipe();
  }

  public signUp(credentials: ISignUp): Observable<any> {
    return this._http.post(`${this._origin}/api/auth/sign-up/`, credentials).pipe();
  }

  public signOut(): Observable<any> {
    return this._http.get(`${this._origin}/api/auth/sign-out/`).pipe(
      tap(res => {
        this._store.dispatch(new TokenActions.DropToken());
        this._store.dispatch(new UserActions.DropUser());
        this._appStorage.removeItem('token');
        if (isPlatformBrowser(this._platformId)) {
          window.localStorage.removeItem('user');
        }
        return res;
      }),
    );
  }

  public checkUserExists(email: string, username: string): Observable<any> {
    return this._http.get(`${this._origin}/api/auth/find-by-credentials/`, {
      params: {email, username}
    });
  }
}
