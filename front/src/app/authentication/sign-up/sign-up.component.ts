import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { isPlatformBrowser } from '@angular/common';

import { Store } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { debounceTime } from 'rxjs/operators';

import { AppStorage } from '../../../forStorage/universal.inject';
import { IUserResponse } from '../shared/models/authentication.model';
import * as TokenActions from '../shared/actions/token.action';
import * as UserActions from '@shared/user/actions/user.action';
import { IToken } from '../shared/models/token.model';
import { IUser } from '@shared/user/models/user.model';
import { TranslatesService } from '@shared/translates/translates.service';
import { AuthenticationService } from '../shared/services/authentication.service';


interface AppState {
  token: IToken;
  user: IUser;
}

@Component({
  selector: 'ts-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit {

  public signUpForm: FormGroup;

  private _cacheCredentials: { email: string, username: string } = {} as { email: string, username: string };

  constructor(@Inject(AppStorage) private _appStorage: Storage,
              @Inject(PLATFORM_ID) private _platformId: Object,
              private _store: Store<AppState>,
              private _toastr: ToastrService,
              private _translatesService: TranslatesService,
              private _authenticationService: AuthenticationService,
              private _router: Router,
              private _fb: FormBuilder) { }

  ngOnInit() {
    // TODO: create pattern to validate at least one number
    this.signUpForm = this._fb.group({
      email: ['', Validators.email],
      username: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(40),
          Validators.pattern(/^[0-9a-zA-Z]*$/),
        ],
      ],
      password: ['', Validators.minLength(8)],
      confirm_password: ['', Validators.minLength(8)],
    });
    this.signUpForm.valueChanges.pipe(
      debounceTime(500),
    ).subscribe(form => {
      if (form.email !== this._cacheCredentials.email || form.username !== this._cacheCredentials.username) {
        this._authenticationService.checkUserExists(
          form.email,
          form.username,
        ).subscribe((res2: { email: boolean, username: boolean }) => {
          if (res2.email) {
            this.signUpForm.controls['email'].setErrors({ 'user-exists': true });
          }
          if (res2.username) {
            this.signUpForm.controls['username'].setErrors({ 'user-exists': true });
          }
        });
      }
      this._cacheCredentials = {
        email: form.email,
        username: form.username,
      };
      if (form.password !== form.confirm_password) {
        this.signUpForm.controls['password'].setErrors({ 'password-dont-match': true });
        this.signUpForm.controls['confirm_password'].setErrors({ 'password-dont-match': true });
      }
    });
  }

  public submit() {
    if (this.signUpForm.valid && this.signUpForm.value.password === this.signUpForm.value.confirm_password) {
      this._authenticationService.signUp(this.signUpForm.value).subscribe((res: IUserResponse) => {
        this._store.dispatch(new TokenActions.SetToken(res.token));
        this._store.dispatch(new UserActions.SetUser(res.user));
        this._appStorage.setItem('token', res.token);
        if (isPlatformBrowser(this._platformId)) {
          window.localStorage.setItem('user', JSON.stringify(res.user));
        }
        this._router.navigate(['/dashboard']);
      }, err => {
        this._translatesService.get(['TOASTR.ERRORS.AUTHENTICATION.SIGN_UP', 'TOASTR.ERRORS.TITLE']).subscribe(res => {
          this._toastr.error(res['TOASTR.ERRORS.AUTHENTICATION.SIGN_UP'], res['TOASTR.ERRORS.TITLE']);
        });
      });
    }
  }
}
