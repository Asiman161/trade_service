import { Routes } from '@angular/router';

import { SignUpComponent } from './sign-up/sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { AuthenticationComponent } from './authentication.component';

export const routes: Routes = [
  {
    path: '', component: AuthenticationComponent, children: [
      {
        path: 'sign-up', component: SignUpComponent, data: { title: 'REGISTRATION' },
      },
      {
        path: 'sign-in', component: SignInComponent, data: { title: 'AUTHORIZATION' },
      },
    ],
  },
];
