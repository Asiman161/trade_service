import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';

import { AppStorage } from '../../../forStorage/universal.inject';
import { IUserResponse } from '../shared/models/authentication.model';
import * as TokenActions from '../shared/actions/token.action';
import * as UserActions from '@shared/user/actions/user.action';
import { IToken } from '../shared/models/token.model';
import { IUser } from '@shared/user/models/user.model';
import { isPlatformBrowser } from '@angular/common';
import { AuthenticationService } from '../shared/services/authentication.service';

interface AppState {
  token: IToken;
  user: IUser;
}

@Component({
  selector: 'ts-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent implements OnInit {

  public signInForm: FormGroup;

  constructor(@Inject(AppStorage) private _appStorage: Storage,
              @Inject(PLATFORM_ID) private _platformId: Object,
              private _store: Store<AppState>,
              private _authenticationService: AuthenticationService,
              private _fb: FormBuilder,
              private _router: Router) {
  }

  ngOnInit(): void {
    this.signInForm = this._fb.group({
      email: [''],
      password: ['', Validators.minLength(8)],
    });
  }

  public submit(): void {
    this._authenticationService.signIn(this.signInForm.value).subscribe((res: IUserResponse) => {
      this._appStorage.setItem('token', res.token);
      this._store.dispatch(new TokenActions.SetToken(res.token));
      this._store.dispatch(new UserActions.SetUser(res.user));
      if (isPlatformBrowser(this._platformId)) {
        window.localStorage.setItem('user', JSON.stringify(res.user));
      }
      this._router.navigate(['/dashboard']);
    }, err => {
      console.log('err in component', err);
    });
  }

}
