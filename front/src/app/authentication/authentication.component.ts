import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ts-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss'],
})
export class AuthenticationComponent implements OnInit {

  public title: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.title = this.route.firstChild.snapshot.data['title'];
  }

}
