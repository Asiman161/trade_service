import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { StoreModule } from '@ngrx/store';
import { MatIconModule } from '@angular/material';
import { CookieService } from 'ngx-cookie-service';
import { MetaLoader, MetaModule, MetaSettings, MetaStaticLoader, PageTitlePositioning } from '@ngx-meta/core';
import { ToastrModule } from 'ngx-toastr';
import { OverlayModule } from '@angular/cdk/overlay';
import { DeviceDetectorModule } from 'ngx-device-detector';

import { PageNotFoundComponent } from './404/page-not-found.component';
import { AppComponent } from './app.component';
import { ROUTES } from './app.routes';
import * as fromToken from './authentication/shared/reducers/token.reducer';
import * as fromUser from '@shared/user/reducers/user.reducer';
import { AuthenticationService } from './authentication/shared/services/authentication.service';

export function metaFactory(): MetaLoader {
  const setting: MetaSettings = {
    // callback: (key: string) => key,
    pageTitlePositioning: PageTitlePositioning.PrependPageTitle,
    pageTitleSeparator: ' | ',
    applicationName: 'Trade Service',
    applicationUrl: 'https://trade-service.com/',
    defaults: {
      title: 'Trade Service',
      description: 'we can help you to make trades',
      'og:site_name': 'Trade Service',
      'og:type': 'website',
      'og:locale': 'ru_RU',
      'og:locale:alternate': 'en_US'
    }
  };
  return new MetaStaticLoader(setting);
}



@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
  ],
  imports: [
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'csrftoken',
      headerName: 'X-CSRFToken',
    }),
    ReactiveFormsModule,
    MatIconModule,
    OverlayModule,
    DeviceDetectorModule.forRoot(),
    BrowserModule.withServerTransition({ appId: 'ts-root' }),
    RouterModule.forRoot(ROUTES, { useHash: false, initialNavigation: 'enabled' }),
    MetaModule.forRoot({
      provide: MetaLoader,
      useFactory: (metaFactory),
      deps: []
    }),
    StoreModule.forRoot({
      token: fromToken.reducer,
      user: fromUser.reducer,
    }),
    ToastrModule.forRoot({progressBar: true}),
  ],
  providers: [CookieService, AuthenticationService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
