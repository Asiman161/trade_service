from django.contrib import admin

from .models import User


class UserModelAdmin(admin.ModelAdmin):
    list_display = ['email', 'username', 'first_name', 'last_name', 'last_login', 'is_staff', 'is_active']
    list_display_links = ['email']
    list_filter = ['email', 'username']
    search_fields = ['email', 'username']

    class Meta:
        model = User


admin.site.register(User, UserModelAdmin)
