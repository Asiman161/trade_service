from django.urls import path
from django.views.decorators.csrf import ensure_csrf_cookie

from rest_framework_jwt.views import (
    obtain_jwt_token,
    refresh_jwt_token,
    verify_jwt_token
)

from . import views

urlpatterns = [
    path('sign-up/', ensure_csrf_cookie(views.SignUpAPIView.as_view())),
    path('find-by-credentials/', views.EmailAPIView.as_view()),
    path('sign-in/', ensure_csrf_cookie(obtain_jwt_token)),
    path('sign-out/', views.SignOutAPIView.as_view()),
    path('refresh-token/', refresh_jwt_token),
    path('validate-token/', verify_jwt_token),
]
