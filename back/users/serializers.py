from uuid import uuid4

from django.db.models import PositiveIntegerField
from rest_framework.exceptions import ValidationError
from rest_framework.fields import EmailField, CharField
from rest_framework.serializers import ModelSerializer
from rest_framework_jwt.serializers import jwt_payload_handler, jwt_encode_handler

from users.models import User


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = [
            'email',
            'username',
            'first_name',
            'last_name',
        ]


class UserShortSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = [
            'username',
        ]


class UserCreateSerializer(ModelSerializer):
    id = PositiveIntegerField()
    email = EmailField(max_length=255)
    first_name = CharField(read_only=True)
    last_name = CharField(read_only=True)
    password = CharField(write_only=True)
    confirm_password = CharField(label='Email Address', write_only=True)
    token = CharField(max_length=255, write_only=True, default='')

    class Meta:
        model = User
        fields = [
            'id',
            'email',
            'username',
            'first_name',
            'last_name',
            'password',
            'confirm_password',
            'token'
        ]

    def validate_email(self, value):
        email = value
        user_qs = User.objects.filter(email=email)
        if user_qs.exists():
            raise ValidationError('This user has already registered.')
        return value

    def validate_password(self, value):
        data = self.get_initial()
        password = value
        confirm_password = data.get('confirm_password')
        if password != confirm_password:
            raise ValidationError('Passwords must match.')
        return value

    def create(self, validated_data):
        email = validated_data['email']
        password = validated_data['password']
        username = validated_data['username']
        user = User(
            email=email,
            username=username
        )
        user.set_password(password)
        user.jwt_secret = uuid4()
        user.save()
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        self.token = token

        return user

    def get_token(self):
        return self.token
