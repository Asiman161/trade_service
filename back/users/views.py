from uuid import uuid4

from django.db.models import Q
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED
from rest_framework.views import APIView
from rest_framework_jwt.settings import api_settings

from api.users_detail.models import UserDetail
from users.models import User
from .serializers import UserCreateSerializer


class SignUpAPIView(CreateAPIView):
    permission_classes = [AllowAny]
    serializer_class = UserCreateSerializer
    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        user_detail = UserDetail(user_id=serializer.data['id'])
        user_detail.save()
        headers = self.get_success_headers(serializer.data)
        return Response({'user': serializer.data, 'token': serializer.token}, status=HTTP_201_CREATED, headers=headers)


class SignOutAPIView(APIView):
    def get(self, request):
        request.user.jwt_secret = uuid4()
        request.user.save()
        return Response(status=HTTP_200_OK)


class EmailAPIView(APIView):
    permission_classes = [AllowAny]

    def get(self, request):
        # TODO: remove check username. ONLY EMAIL.
        # remove after implementing editing user settings
        email = request.query_params.get('email', None)
        username = request.query_params.get('username', None)
        queryset = User.objects.filter(Q(email__iexact=email) | Q(username__iexact=username)).values('email', 'username')
        res = {}
        if queryset:
            res['email'] = True if email.lower() == queryset[0]['email'].lower() else False
            res['username'] = True if username.lower() == queryset[0]['username'].lower() else False
        return Response(data=res)
