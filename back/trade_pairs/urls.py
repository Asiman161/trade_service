from django.urls import path

from .views import TradePairList

urlpatterns = [
    path('', TradePairList.as_view(), name='index'),
]
