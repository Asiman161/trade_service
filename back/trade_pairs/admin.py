from django.contrib import admin

from .models import TradePair


class TradePairModelAdmin(admin.ModelAdmin):
    fields = ('pair', 'exchange', 'is_active', 'is_deleted')
    list_display = ('pair', 'get_exchanges', 'is_active', 'is_deleted')
    list_display_links = ['pair']

    class Meta:
        model = TradePair

    def get_exchanges(self, obj):
        return '\n'.join([s.name for s in obj.exchange.all()])
    get_exchanges.short_description = 'Exchanges'


admin.site.register(TradePair, TradePairModelAdmin)
