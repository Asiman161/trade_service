from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny

from .models import TradePair
from .serializers import TradePairSerializer


class TradePairList(ListAPIView):
    serializer_class = TradePairSerializer
    permission_classes = [AllowAny]
    queryset = TradePair.objects.filter(is_deleted=False)
