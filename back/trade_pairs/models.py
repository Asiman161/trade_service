from django.db import models

from exchanges.models import Exchange


class TradePair(models.Model):
    pair = models.CharField(db_index=True, max_length=15)
    exchange = models.ManyToManyField(Exchange)

    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return str(self.pair)
