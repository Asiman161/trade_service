from rest_framework.serializers import (
    ModelSerializer,
)

from exchanges.serializers import ExchangeSerializer
from .models import TradePair


class TradePairSerializer(ModelSerializer):
    exchange = ExchangeSerializer(read_only=True, many=True)

    class Meta:
        model = TradePair
        fields = [
            'pair',
            'exchange'
        ]
