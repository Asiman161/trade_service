from django.apps import AppConfig


class TradePairsConfig(AppConfig):
    name = 'trade_pairs'
