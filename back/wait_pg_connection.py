from time import sleep
from os import environ

import psycopg2

connection = None
PG_ATTEMPTS_COUNT = environ.get('PG_ATTEMPTS_COUNT') or 10
PG_ATTEMPT_TIME = environ.get('PG_ATTEMPT_TIME') or 3


def connect():
    return psycopg2.connect(
        database=environ.get('POSTGRES_DB'),
        user=environ.get('POSTGRES_USER'),
        password=environ.get('POSTGRES_PASSWORD'),
        host=environ.get('POSTGRES_HOST'),
        port=environ.get('POSTGRES_PORT')
    )


for i in range(PG_ATTEMPTS_COUNT):
    try:
        connection = connect()
        break
    except psycopg2.Error:
        print('waiting pg')
        sleep(PG_ATTEMPT_TIME)
if connection:
    print('pg connected')
    connection.close()
else:
    raise Exception('can\'t connect to pg')
