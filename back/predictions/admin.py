from django.contrib import admin

from .models import Predict


class PredictModelAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'order_type', 'views_count', 'comments_count', 'created_at', 'modified_at', 'is_active',
                    'is_deleted')
    list_display_links = ['__str__']

    class Meta:
        model = Predict


admin.site.register(Predict, PredictModelAdmin)
