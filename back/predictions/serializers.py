import re

from rest_framework.exceptions import ValidationError
from rest_framework.fields import CharField, DateTimeField
from rest_framework.serializers import (
    ModelSerializer,
)

from trade_pairs.serializers import TradePairSerializer
from users.serializers import UserShortSerializer
from .models import Predict
from exchanges.serializers import ExchangeSerializer


class PredictSerializer(ModelSerializer):
    trade_pair = TradePairSerializer(read_only=True)
    exchange = ExchangeSerializer(read_only=True)
    user = UserShortSerializer(read_only=True)

    class Meta:
        model = Predict
        fields = [
            'img_url',
            'order_type',
            'views_count',
            'comments_count',
            'likes_count',
            'targets',
            'trade_pair',
            'short_description',
            'description',
            'exchange',
            'user',
            'created_at',
            'modified_at',
        ]


class PredictCreateSerializer(ModelSerializer):
    trade_pair = CharField()
    exchange = CharField()
    user = UserShortSerializer(read_only=True)
    created_at = DateTimeField(read_only=True)

    class Meta:
        model = Predict
        fields = [
            'img_url',
            'order_type',
            'targets',
            'trade_pair',
            'short_description',
            'description',
            'exchange',
            'user',
            'created_at',
        ]

    def to_representation(self, instance):
        data = super(PredictCreateSerializer, self).to_representation(instance)
        data['trade_pair'] = {
            'exchange': {'name': data['exchange']},
            'pair': data['trade_pair']
        }
        data['exchange'] = {'name': data['exchange']}
        return data

    def validate_targets(self, value):
        pattern = re.compile("^(\d{1,15}\.\d{1,15}|(?!0)\d{1,15})$")
        if pattern.match(value['stop']):
            for obj in value['target_zones']:
                if obj['description'] != '' and pattern.match(obj['tz_min']) and (
                        obj['tz_max'] == '' or pattern.match(obj['tz_max'])
                ):
                    continue
                else:
                    # TODO: change error message to translate key
                    # raise ValidationError('ERRORS.VALIDATION.WRONG_DATA_FORMAT')
                    raise ValidationError('description empty or target has wrong format')
        return value
