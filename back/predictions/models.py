from django.contrib.postgres import fields
from django.db import models

from exchanges.models import Exchange
from trade_pairs.models import TradePair
from users.models import User


class Predict(models.Model):
    img_url = models.CharField(max_length=255)
    order_type = models.BooleanField(help_text='True if LONG and False if SHORT')
    views_count = models.IntegerField(default=0)
    comments_count = models.IntegerField(default=0)
    likes_count = models.IntegerField(default=0)
    short_description = models.CharField(max_length=255, default='')
    description = models.TextField()

    targets = fields.JSONField()  # {stop: string, target_zones: [{description: string, tz_min: string, tz_max}]]

    trade_pair = models.ForeignKey(TradePair, on_delete=models.CASCADE)
    exchange = models.ForeignKey(Exchange, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.trade_pair.pair + ' / ' + self.exchange.name
