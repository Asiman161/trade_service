from rest_framework.generics import ListAPIView, CreateAPIView

from rest_framework.permissions import AllowAny

from trade_pairs.models import TradePair
from exchanges.models import Exchange
from .serializers import PredictSerializer, PredictCreateSerializer
from .models import Predict


class PredictList(ListAPIView):
    serializer_class = PredictSerializer
    permission_classes = [AllowAny]
    queryset = Predict.objects.filter(is_deleted=False)


class PredictCreate(CreateAPIView):
    queryset = Predict.objects.all()
    serializer_class = PredictCreateSerializer

    def perform_create(self, serializers):
        exchange = Exchange.objects.get(name=self.request.data['exchange'])
        trade_pair = TradePair.objects.get(
            pair=self.request.data['trade_pair'].upper(),
            exchange=exchange
        )
        serializers.save(trade_pair=trade_pair, exchange=exchange, user=self.request.user)
