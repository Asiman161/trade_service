from django.urls import path

from .views import PredictList, PredictCreate

urlpatterns = [
    path('', PredictList.as_view(), name='index'),
    path('create', PredictCreate.as_view(), name='create'),
]
