from django.contrib import admin

from .models import Exchange


class ExchangeModelAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'created_at', 'modified_at', 'is_active', 'is_deleted')
    list_display_links = ['__str__']

    class Meta:
        model = Exchange


admin.site.register(Exchange, ExchangeModelAdmin)
