from django.apps import AppConfig


class TradesConfig(AppConfig):
    name = 'api.trades'
