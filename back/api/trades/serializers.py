import re

from rest_framework.exceptions import ValidationError
from rest_framework.fields import CharField
from rest_framework.serializers import ModelSerializer

from .models import Trade


class TradeCreateSerializer(ModelSerializer):
    exchange = CharField(write_only=True)
    pattern = re.compile("^(\d{1,15}\.\d{1,15}|(?!0)\d{1,15})$")

    class Meta:
        model = Trade
        fields = [
            'exchange',
            'symbol',
            'side',
            'look_by',
            'take_position',
            'stop_position',
            'price',
            'quantity',
            'buy_direction',
            'type',
        ]

    def validate_buy_direction(self, value):
        if value == 'DOWN' or value == 'UP':
            return value
        raise ValidationError('buy_direction can be \'DOWN\' or \'UP\' only.')

    def validate_take_position(self, value):
        if not self.pattern.match(value):
            raise ValidationError('Wrong take_position format.')
        return value

    def validate_stop_position(self, value):
        if not self.pattern.match(value):
            raise ValidationError('Wrong stop_position format.')
        return value

    def validate_price(self, value):
        if not self.pattern.match(value):
            raise ValidationError('Wrong price format.')
        return value

    def validate_quantity(self, value):
        if not self.pattern.match(value):
            raise ValidationError('Wrong quantity format.')
        return value
