import requests

from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from api.trades.serializers import TradeCreateSerializer
from trade_service.settings.settings import BOT_ACCESS_HEADER, BOT_ADDRESS


class TradeView(APIView):
    def get(self, request):
        r = requests.get('{}/trades/{}/'
                         .format(BOT_ADDRESS, request.user.id), headers=BOT_ACCESS_HEADER)
        if r.status_code == HTTP_200_OK:
            return Response(r.json(), r.status_code)
        else:
            return Response(r, r.status_code)

    def post(self, request):
        serializer = TradeCreateSerializer(data=request.data)
        if serializer.is_valid():
            data = serializer.validated_data
            data['user_id'] = request.user.id
            r = requests.post('{}/trades/'.format(BOT_ADDRESS), json=data, headers=BOT_ACCESS_HEADER)
            if r.status_code == HTTP_200_OK:
                local_serializer = TradeCreateSerializer(data=r.json())
                if local_serializer.is_valid():
                    local_serializer.save()
                return Response(r.json(), HTTP_200_OK)
            else:
                return Response(r.json(), r.status_code)
        else:
            return Response(status=HTTP_400_BAD_REQUEST)


class TradeIdView(APIView):
    def delete(self, request, trade_id):
        r = requests.delete('{}/trades/{}/{}/'
                            .format(BOT_ADDRESS, request.user.id, trade_id), headers=BOT_ACCESS_HEADER)
        if r.status_code == HTTP_200_OK:
            return Response(r.json(), r.status_code)
        return Response(r, r.status_code)
