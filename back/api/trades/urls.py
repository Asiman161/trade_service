from django.urls import path

from .views import TradeView, TradeIdView

urlpatterns = [
    path('', TradeView.as_view(), name='index'),
    path('<int:trade_id>/', TradeIdView.as_view(), name='index'),
]
