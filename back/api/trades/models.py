from django.db import models


class Trade(models.Model):
    symbol = models.CharField(db_index=True, max_length=8)
    side = models.CharField(max_length=4)
    look_by = models.CharField(max_length=10)
    take_position = models.CharField(max_length=15)
    stop_position = models.CharField(max_length=15)
    price = models.CharField(max_length=15)
    quantity = models.CharField(max_length=15)
    buy_direction = models.CharField(max_length=4)
    type = models.CharField(max_length=10)
    close_by_stop = models.BooleanField(default=False, blank=True)
    is_cancel = models.BooleanField(default=False, blank=True)
    is_done = models.BooleanField(default=False, blank=True)

    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return str(self.symbol)
