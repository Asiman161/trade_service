from django.urls import path, include

urlpatterns = [
    path('trades/', include('api.trades.urls')),
    path('users/', include('api.users_detail.urls')),
]
