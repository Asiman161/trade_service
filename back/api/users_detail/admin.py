from django.contrib import admin

from .models import UserDetail


class UserDetailModelAdmin(admin.ModelAdmin):
    fields = ('user', 'key', 'secret')
    list_display = ('user', 'key')

    class Meta:
        model = UserDetail

    def get_secret(self, obj):
        return '\n'.join([s.name for s in obj.exchange.all()])

    get_secret.short_description = 'Secret'


admin.site.register(UserDetail, UserDetailModelAdmin)
