from django.db import models

from users.models import User


class UserDetail(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE
    )
    key = models.CharField(blank=True, null=True, max_length=255)
    secret = models.CharField(blank=True, null=True, max_length=255)
