from rest_framework.serializers import ModelSerializer

from .models import UserDetail


class UserDetailSerializer(ModelSerializer):
    class Meta:
        model = UserDetail
        fields = [
            'key',
            'secret'
        ]


class UserDetailHiddenSerializer(ModelSerializer):
    class Meta:
        model = UserDetail
        fields = [
            'key',
            'secret',
        ]

    def to_representation(self, instance):
        data = super(UserDetailHiddenSerializer, self).to_representation(instance)
        if data['secret'] and len(data['secret']) > 2:  # if it has less than 2 symbols it means that it isn't a secret
            data['secret'] = '**********'
        return data
