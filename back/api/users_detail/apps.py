from django.apps import AppConfig


class UsersDetailConfig(AppConfig):
    name = 'api.users_detail'
