import requests

# Create your views here.
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from api.users_detail.serializers import UserDetailHiddenSerializer, UserDetailSerializer
from trade_service.settings.settings import BOT_ACCESS_HEADER, BOT_ADDRESS
from .models import UserDetail


class UserDetailView(APIView):

    def get(self, request):
        user = UserDetail.objects.get(user=self.request.user)
        serializer = UserDetailHiddenSerializer(user)
        return Response(serializer.data, HTTP_200_OK)

    def patch(self, request):
        user = UserDetail.objects.get(user=self.request.user)
        serializer = UserDetailSerializer(user, data=request.data)
        if serializer.is_valid():
            headers = BOT_ACCESS_HEADER
            data = serializer.validated_data
            data['user_id'] = self.request.user.id
            r = requests.get(
                '{}/users/{}/'.format(BOT_ADDRESS, self.request.user.id),
                headers=headers
            )
            if r.status_code == HTTP_200_OK:
                r = requests.patch('{}/users/{}/'
                                   .format(BOT_ADDRESS, self.request.user.id),
                                   json=data,
                                   headers=headers)
                if r.status_code == HTTP_200_OK:
                    res = r.json()
                    serializer.save(key=res['key'], secret=res['secret'], user=self.request.user)
                    return Response({'key': res['key'], 'secret': res['secret']}, HTTP_200_OK)
                else:
                    return Response(r.json(), r.status_code)
            elif r.status_code == HTTP_404_NOT_FOUND:
                r = requests.post('{}/users/'.format(BOT_ADDRESS), json=data, headers=headers)
                if r.status_code == HTTP_200_OK:
                    res = r.json()
                    serializer.save(key=res['key'], secret=res['secret'], user=self.request.user)
                    return Response({'key': res['key'], 'secret': res['secret']}, HTTP_200_OK)
                else:
                    return Response(r.json(), r.status_code)
            else:
                return Response(r.json(), r.status_code)
        else:
            return Response({}, HTTP_400_BAD_REQUEST)

    def delete(self, request):
        user = UserDetail.objects.get(user=self.request.user)
        headers = BOT_ACCESS_HEADER
        r = requests.delete(
            '{}/users/{}/'.format(BOT_ADDRESS, self.request.user.id),
            headers=headers
        )
        serializer = UserDetailSerializer(user, data=r.json())
        if r.status_code == HTTP_200_OK and serializer.is_valid():
            serializer.save(key='', secret='', user=self.request.user)
            return Response(serializer.data, HTTP_200_OK)

        return Response(status=HTTP_400_BAD_REQUEST)
