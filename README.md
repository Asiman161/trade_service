# How to start

write your own `.env` file. You can use `.env_example` to make it right  

in console you need to write: 

`docker-compose up --build`

`docker ps` - you'll see a list of container

`docker exec -ti %django_container_id% python manage.py createsuperuser`

after it you need to login as admin and create at least 1 exchange and trade pair in admin menu

now you can use service, enjoy!